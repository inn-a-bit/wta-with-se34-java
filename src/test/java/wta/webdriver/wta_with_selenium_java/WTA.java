package wta.webdriver.wta_with_selenium_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WTA {
	private static WebDriver driver;
	private static WebDriverWait wait;
	private static ChromeOptions options;
	SimpleActionsPage sap = new SimpleActionsPage();
	SummaryActionsPage sum_ap = new SummaryActionsPage();
	String parentWindow = "";

	@BeforeClass
	public static void beforeClassTest() {
		String baseURL = "https://wta-with-se34-webdriver.info";
		System.setProperty("webdriver.chrome.driver", "/Users/innakomarova/Documents/Selenium/chromedriver");
		options = new ChromeOptions();
		options.addArguments("--start-maximazed");
		driver = new ChromeDriver(options);
		driver.get(baseURL);
		wait = new WebDriverWait(driver, 10);
	}

	@AfterClass
	public static void afterClassTest() throws Exception {
		driver.close();
	}

	@Test
	public void test_a_a_fnu_resume_summary() throws Exception {
		sap.clickLink(driver, wait, "/html/body/div[1]/table/tbody/tr[3]/td/a");
		parentWindow = sap.switchToNewWindow(driver);
		sap.switchToIFrame(driver, "iFrame3");
		sap.clickLink(driver, wait, "//*[@id=\"skills\"]/tbody/tr[2]/td[1]/a");
		String parentWindow2 = sap.switchToNewWindow(driver);
		Point point = new Point(200, 100);
		driver.manage().window().setPosition(point);
		Dimension d = new Dimension(700, 1100);
		driver.manage().window().setSize(d);
		sap.switchToParentWindow(driver, parentWindow2);
	}

	@Test
	public void test_a_checkbox_summary() throws Exception {
		sap.switchToIFrame(driver, "iFrame4");
		sap.scrollByPixel(driver);
		WebElement element = sap.clickCheckbox(driver, wait, "//*[@id=\"skills\"]/tbody/tr[2]/td[1]/input");
		AssertJUnit.assertEquals("true",element.getAttribute("checked"));
	}

	@Test
	public void test_b_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "15", "//*[@id=\"skills\"]/tbody/tr[2]/td[3]/input");
		AssertJUnit.assertTrue(text.equals("15"));
	}

	@Test
	public void test_c_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "2020", "//*[@id=\"skills\"]/tbody/tr[2]/td[4]/input");
		AssertJUnit.assertTrue(text.equals("2020"));
	}

	@Test
	public void test_d_dropDownByXPath() throws Exception {
		String value = sap.selectFromDropDownByXpath(driver, wait, 2, "//*[@id=\"skills\"]/tbody/tr[2]/td[5]/select");
		AssertJUnit.assertTrue(value.equals("Advanced"));
	}

	@Test
	public void test_e_radioButton() throws Exception {
		String value = sap.selectRadioButton(driver, wait,
				"/html/body/div[1]/form/table[2]/tbody/tr[2]/td[6]/input[2]");
		AssertJUnit.assertTrue(value.equals("Update"));
	}

	@Test
	public void test_f_checkbox_summary() throws Exception {
		WebElement element = sap.clickCheckbox(driver, wait, "//*[@id=\"skills\"]/tbody/tr[4]/td[1]/input");
		AssertJUnit.assertEquals("true",element.getAttribute("checked"));
	}

	@Test
	public void test_g_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "1", "//*[@id=\"skills\"]/tbody/tr[4]/td[3]/input");
		AssertJUnit.assertTrue(text.equals("1"));
	}

	@Test
	public void test_h_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "2020", "//*[@id=\"skills\"]/tbody/tr[4]/td[4]/input");
		AssertJUnit.assertTrue(text.equals("2020"));
	}

	@Test
	public void test_i_dropDownByXPath() throws Exception {
		String value = sap.selectFromDropDownByXpath(driver, wait, 4, "//*[@id=\"skills\"]/tbody/tr[4]/td[5]/select");
		AssertJUnit.assertTrue(value.equals("Novice"));
	}

	@Test
	public void test_j_radioButton() throws Exception {
		String value = sap.selectRadioButton(driver, wait,
				"/html/body/div[1]/form/table[2]/tbody/tr[4]/td[6]/input[2]");
		AssertJUnit.assertTrue(value.equals("Update"));
	}
	
	@Test
	public void test_ja_findElementInTable() throws Exception {
		sap.switchToParentWindow(driver);
		sap.switchToIFrame(driver, "iFrame5");
		boolean value = sum_ap.findElementInTable(driver, wait, "Selenium");
		AssertJUnit.assertEquals(true, value);
	}

	@Test
	public void test_k_click_checkbox() throws Exception {
		sap.switchToParentWindow(driver);
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 2);
		parentWindow = sap.switchToNewWindow(driver);
		WebElement element = sap.clickCheckbox(driver, wait, "/html/body/div[1]/div[2]/table/tbody/tr[2]/td/label/span");
		AssertJUnit.assertNotNull(element);	
	}

	@Test
	public void test_l_click_checkbox_by_index() throws Exception {
		WebElement element = sap.clickCheckboxByIndex(driver, wait, 2);
		AssertJUnit.assertNotNull(element);
	}

	@Test
	public void test_m_uncheck_checkbox() throws Exception {
		WebElement element = sap.uncheckCheckBox(driver, wait, "/html/body/div[1]/div[2]/table/tbody/tr[2]/td/label/span");
		AssertJUnit.assertEquals(null, element);
	}

	@Test
	public void test_n_uncheck_checkbox_by_index() throws Exception {
		WebElement element = sap.uncheckCheckBoxByIndex(driver, wait, 2);
		AssertJUnit.assertEquals(false,element.isSelected());	
	}

	@Test
	public void test_o_click_radio_button() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 3);
		parentWindow = sap.switchToNewWindow(driver);
		sap.selectRadioButton(driver, wait, 2);
		sap.selectRadioButton(driver, wait, 1);
	}

	@Test
	public void test_p_populate_textbox() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 4);
		parentWindow = sap.switchToNewWindow(driver);
		sap.setText(driver, wait, "Елена", "first_name_2");
		sap.setText(driver, wait, "Прекрасная", "last_name");
		sap.setText(driver, wait, "test@yahoo.com", "email_id");
	}

	@Test
	public void test_r_select_from_dropdown() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 5);
		parentWindow = sap.switchToNewWindow(driver);
		String value = sap.selectFromDropDown(driver, wait, 2, "skill-level");
		AssertJUnit.assertEquals("Advanced", value);
	}

	@Test
	public void test_s_click_submit() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 6);
		parentWindow = sap.switchToNewWindow(driver);
		String value = sap.clickSubmitWithAlert(driver, wait, "//*[@id=\"submit_it\"]");
		AssertJUnit.assertEquals("Don't forget to test alert!!", value);
	}

	@Test
	public void test_t_findElementInTable() throws Exception {

		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 7);
		parentWindow = sap.switchToNewWindow(driver);
		sap.switchToIFrame(driver, "iFrame5");
		boolean value = sum_ap.findElementInTable(driver, wait, "Selenium");
		AssertJUnit.assertEquals(true, value);
	}

	@Test
	public void test_tt_click_link() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 8);
		parentWindow = sap.switchToNewWindow(driver);
		sap.clickLink(driver, wait, "/html/body/div[2]/h3[2]/a");
		String parentWindow2 = sap.switchToNewWindow(driver);
		Point point = new Point(200, 100);
		driver.manage().window().setPosition(point);
		Dimension d = new Dimension(700, 1100);
		driver.manage().window().setSize(d);
		sap.switchToParentWindow(driver, parentWindow2);
		sap.switchToParentWindow(driver, parentWindow);
	}
}
