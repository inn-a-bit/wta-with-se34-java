package wta.webdriver.wta_with_selenium_java;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SimpleActionsPage {
	public WebElement clickCheckbox(WebDriver driver, WebDriverWait wait, String xpath) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		Actions actions = new Actions(driver);
		WebElement element = null;
		if (!driver.findElement(By.xpath(xpath)).isSelected()) {
			element = driver.findElement(By.xpath(xpath));
			actions.moveToElement(element).click().build().perform();
		}
		return element;
	}

	public WebElement uncheckCheckBox(WebDriver driver, WebDriverWait wait, String xpath) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		Actions actions = new Actions(driver);
		WebElement element = null;
		if (driver.findElement(By.xpath(xpath)).isSelected()) {
			element = driver.findElement(By.xpath(xpath));
			actions.moveToElement(element).click().build().perform();
		}
		return element;
	}

	public WebElement clickCheckboxByIndex(WebDriver driver, WebDriverWait wait, int index) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span")));
		Actions actions = new Actions(driver);
		WebElement element = null;
		if (!driver.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span"))
				.isSelected()) {
			element = driver
					.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span"));
			actions.moveToElement(element).click().build().perform();
		}
		return element;
	}
	
	public WebElement uncheckCheckBoxByIndex(WebDriver driver, WebDriverWait wait, int index) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span")));
		Actions actions = new Actions(driver);
		WebElement element = null;
		element = driver
				.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span"));
		actions.moveToElement(element).click().build().perform();
		return element;
	}

	public void clickLink(WebDriver driver, WebDriverWait wait, int index) throws Exception {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[@id=\"exploring\"]/ul/li[" + index + "]/a")));
		WebElement element = null;
		element = driver.findElement(By.xpath("//*[@id=\"exploring\"]/ul/li[" + index + "]/a"));
		scrollIntoView(driver, element);
		element.click();
	}

	public void clickLink(WebDriver driver, WebDriverWait wait, String xpath) 
	{ 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath))); 
		WebElement element = driver.findElement(By.xpath(xpath));	
		element.click(); 
	}
	public void scrollByPixel(WebDriver driver) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	}

	public void scrollIntoView(WebDriver driver, WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].scrollIntoView();", element);
	}

	public void switchToIFrame(WebDriver driver, String iframeId) {
		WebElement iframeSwitch = driver.findElement(By.id(iframeId));
		driver.switchTo().frame(iframeSwitch);
	}

	public void switchToParentWindow(WebDriver driver) {
		driver.switchTo().defaultContent();
	}

	public String switchToNewWindow(WebDriver driver) {
		String parentWind = driver.getWindowHandle();
		for (String childWin : driver.getWindowHandles()) {
			driver.switchTo().window(childWin);
		}

		return parentWind;
	}

	public void switchToParentWindow(WebDriver driver, String parentWindow) {
		driver.close();
		driver.switchTo().window(parentWindow);
	}

	public void selectRadioButton(WebDriver driver, WebDriverWait wait, int index) 
	{ 
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath( "/html/body/div/div[2]/table/tbody/tr[" + index + "]/td/label/span")))); 
		WebElement radioBtn = driver.findElement(By.xpath( "/html/body/div/div[2]/table/tbody/tr[" + index + "]/td/label/span")); 
		radioBtn.click(); 
	}
	public String selectRadioButton(WebDriver driver, WebDriverWait wait, String xpath) {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(xpath))));
		WebElement radioBtn = driver.findElement(By.xpath(xpath));
		radioBtn.click();
		return radioBtn.getAttribute("value");
	}

	public String selectFromDropDown(WebDriver driver, WebDriverWait wait, int index, String id_str) 
	{ 
		Select dropDown = new Select(driver.findElement(By.id(id_str))); 
		dropDown.selectByIndex(index); 
		return dropDown.getAllSelectedOptions().get(0).getText();	
	}
	public String selectFromDropDownByXpath(WebDriver driver, WebDriverWait wait, int index, String xpath) {
		Select dropDown = new Select(driver.findElement(By.xpath(xpath)));
		dropDown.selectByIndex(index);
		return dropDown.getAllSelectedOptions().get(0).getText();
	}

	public String clickSubmitWithAlert(WebDriver driver, WebDriverWait wait, String xpath) 
	{ 
		WebElement webel = driver.findElement(By.xpath(xpath)); 
		JavascriptExecutor executor = (JavascriptExecutor)driver; 
		executor.executeScript("arguments[0].click();", webel); 
		wait.until(ExpectedConditions.alertIsPresent()); 
		String alertText = driver.switchTo().alert().getText(); 
		driver.switchTo().alert().accept(); 
		return alertText; 
	}
	public String setText(WebDriver driver, WebDriverWait wait, String text, String element) 
	{ 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element))); 
		driver.findElement(By.id(element)).sendKeys(text); 
		return driver.findElement(By.id(element)).getAttribute("value"); 
	}
}
