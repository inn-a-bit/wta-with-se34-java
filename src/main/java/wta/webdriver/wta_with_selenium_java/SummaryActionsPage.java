package wta.webdriver.wta_with_selenium_java;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SummaryActionsPage {
	public boolean findElementInTable(WebDriver driver, WebDriverWait wait, String text) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"skills\"]/tbody")));
		SimpleActionsPage sap = new SimpleActionsPage();
		WebElement table = driver.findElement(By.xpath("//*[@id=\"skills\"]/tbody"));
		List<WebElement> rowsCltn = table.findElements(By.xpath("//*[@id=\"skills\"]/tbody/tr"));
		WebElement element = null;
		if (rowsCltn.size() > 0) {
			String cell_text = "";
			for (int i = 2; i <= rowsCltn.size(); i++) {
				element = driver.findElement(By.xpath("//*[@id=\"skills\"]/tbody/tr[" + i + "]/td[1]"));
				cell_text = driver.findElement(By.xpath("//*[@id=\"skills\"]/tbody/tr[" + i + "]/td[1]")).getText();
				if (cell_text.contains(text)) {
					sap.scrollIntoView(driver, element);
					((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", element);
					return true;
				}
			}
		}
		return false;
	}

	public String setText(WebDriver driver, WebDriverWait wait, String text, String xpath) {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement element = driver.findElement(By.xpath(xpath));
		element.sendKeys(text);
		String check_text = driver.findElement(By.xpath(xpath)).getAttribute("value");
		return check_text;
	}
}